import { Card } from "antd";
import React from "react";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function ItemMovie({ data }) {
  return (
    <div>
      <Card
        hoverable
        style={{
          width: "100%",
        }}
        cover={
          <img
            className="h-80 w-full object-cover"
            alt="example"
            src={data.hinhAnh}
          />
        }
      >
        <Meta
          title={<p className="text-red-500 truncate">{data.tenPhim}</p>}
          description="www.instagram.com"
        />
        <NavLink to={`/detail/${data.maPhim}`}>
          <button className="w-full py-2 text-center bg-lime-500 text-white mt-5 rounded transition duration-300 cursor-pointer hover:bg-black ">
            Xem Chi Tiết
          </button>
        </NavLink>
      </Card>
    </div>
  );
}
/**
 * {
    "maPhim": 1333,
    "tenPhim": "Trainwreckk",
    "biDanh": "trainwreckk",
    "trailer": "https://www.youtube.com/embed/2MxnhBPoIx4",
    "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/trainwreckk_gp05.jpg",
    "moTa": "<p>Having thought that monogamy was never possible, a commitment-phobic career woman may have to face her fears when she meets a good guy.</p>",
    "maNhom": "GP05",
    "ngayKhoiChieu": "2021-01-11T00:00:00",
    "danhGia": 10,
    "hot": false,
    "dangChieu": true,
    "sapChieu": false
}
 */
