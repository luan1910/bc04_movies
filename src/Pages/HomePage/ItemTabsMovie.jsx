import moment from "moment/moment";
import React from "react";

export default function ItemTabsMovie({ data }) {
  return (
    <div className="p-3 flex space-x-5 border-b border-blue-300">
      <img className="w-28 h-36 object-cover" src={data.hinhAnh} />
      <div className="flex-grow">
        <p>{data.tenPhim}</p>
        <div className="grid grid-cols-3 gap-5">
          {data.lstLichChieuTheoPhim.slice(0, 9).map((gioChieu) => {
            return (
              <div className="p-3 bg-orange-500 text-white text-center">
                {moment(gioChieu.ngayChieuGioChieu).format(
                  "DD-MM-YYYY ~ hh:mm"
                )}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
