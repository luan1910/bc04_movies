import React, { useEffect, useState } from "react";
import { moviesServ } from "../../Services/movieService";
import ItemMovie from "./ItemMovie";
import TabsMovie from "./TabsMovie";

export default function HomePage() {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    moviesServ
      .getListMovie()
      .then((res) => {
        console.log(res);
        setMovies(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderMovies = () => {
    return movies.map((data, index) => {
      return <ItemMovie key={index} data={data} />;
    });
  };
  return (
    <div className="container mx-auto space-x-10 ">
      <div className="grid grid-cols-5 gap-10">{renderMovies()}</div>
      <TabsMovie />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}
