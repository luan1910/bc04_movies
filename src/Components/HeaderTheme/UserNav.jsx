import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../../Services/localService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  console.log("user: ", user);
  let handleLogout = () => {
    // xóa data từ localStorage
    localServ.user.remove();
    // remove data từ redux
    // dispath ({type: SET_USER - payload: null})
    window.location.href = "/login";
  };
  let renderContent = () => {
    if (user) {
      return (
        <>
          <span className="font-medium text-blue-800 underline">
            {user.hoTen}
          </span>
          <button
            onClick={handleLogout}
            className="border rounded border-red-500 px-5 py-2 text-red-500"
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className="border rounded border-black text-black px-5 py-2 hover:bg-black hover:text-white duration-300 transition">
              Đăng nhập
            </button>
          </NavLink>
          <button className="border rounded border-red-500 px-5 py-2 text-red-500">
            Đăng kí
          </button>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
